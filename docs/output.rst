************************************
Metricas, grabaciones y supervisión
************************************

Grabaciones
***********
Todas las campañas que operen con la opción de grabación de llamadas habilitada generan sus archivos con grabaciones para posterior escucha o descarga.
El módulo de *Grabaciones* de OMniLeads permite la búsqueda de grabaciones utilizando los filtros como criterio de búsqueda.


* :ref:`about_recordings`.

Output de las campañas entrantes
********************************

En esta sección se expone y analiza todo lo inherente al "output" generado por las campañas de llamadas entrantes.

* :ref:`about_inbound_camp_reports`.

Output de las campañas salientes
********************************

En esta sección se expone y analiza todo lo inherente al "output" generado por las campañas de llamadas salientes.

* :ref:`about_outbound_camp_reports`.


Reportes general de llamadas
******************************

Esta sección repasa cada informe generado por el reporte general de llamadas. *Reportes -- Llamadas*.

* :ref:`about_general_reports`.

Reportes de agentes
*******************

Esta sección repasa cada informe generado por el reporte general de actividad de agente. *Reportes – Agentes*

* :ref:`about_agent_reports`.

Supervisión realtime
********************

Esta sección repasa el módulo de supervisión de OMniLeads.

* :ref:`about_supervision`.
