.. _about_install:

******************************
Instalación de OMniLeads
******************************
En este capítulo se detallan los tipos de instalación de OMniLeads

* :ref:`about_install_selfhosted`.
* :ref:`about_install_remote`.
* :ref:`about_install_docker_linux`.

El siguiente apartado esta dirigido a usuarios avanzados de Docker, los cuales quieran involucrarse mas con la creacion de imagenes para el proyecto:

* :ref:`about_install_docker_build`.

.. note::

  **Recomendaciones:**

  * Tanto el host como el nodo a instalar tienen que tener conexión buena y estable a internet
  * Que no haya ningún elemento de red para salir a internet (firewall bloqueando puerto 443, proxy)
  * Usar las isos que se recomiendan en la sección Donde se puede Instalar?
  * En caso de fallo de alguna task de ansible volver a correr el script de instalación
  * En caso de que vuelva a fallar levantar un issue a https://gitlab.com/omnileads/ominicontacto/issues especificando distro en la que sucedió y la versión que se intentó instalar
