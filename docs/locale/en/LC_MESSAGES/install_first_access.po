# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2019, omnileads
# This file is distributed under the same license as the OMniLeads package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2019.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version:  OMniLeads\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-09-03 12:40-0300\n"
"PO-Revision-Date: 2019-08-13 15:12-0300\n"
"Last-Translator: \n"
"Language: en\n"
"Language-Team: \n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.6.0\n"

# 810f0d77dba4417b8d0ae9d49982595f
#: ../../install_first_access.rst:4
msgid "Primer acceso a OMniLeads"
msgstr "First access to OMniLeads"

# 5cfc3ecd6b4a4db3af43705b12b6ed9f
#: ../../install_first_access.rst:6
msgid "Para acceder al sistema OMniLeads debe ingresar a:"
msgstr "To access the OMniLeads system you must enter to:"

# fff331ab3a384c1982481a304ea5c9e6
#: ../../install_first_access.rst:8
msgid "https://omnileads-hostname"
msgstr "https://omnileads-hostname"

# 1c877d2a21914d6fa729eec3f5558a1a
#: ../../install_first_access.rst:10
msgid ""
"Nota: El acceso web a OMniLeads debe ser a través del hostname.domain del"
" host. Por lo tanto existen dos posibilidades a la hora de resolver el "
"hostname:"
msgstr ""
"Note: Web access to OMniLeads must be through the hostname.domain of the "
"host. Therefore there are two possibilities when solving the hostname:"

# e6f0884832ff4762994fb749c7ad05dd
#: ../../install_first_access.rst:13
msgid ""
"1 - Que los DNS de la red lo hagan. 2 - Añadir el hostname.domain del "
"host, dentro del archivo de *hosts* (Windows, Linux o Mac de cada PC que "
"tenga que acceder a OMniLeads."
msgstr ""
"1 - That the Network DNS do it. 2 - Add the hostname.domain of the host, "
"inside the file *hosts* (Windows, Linux or Mac of each PC that have "
"access to OMniLeads."

# e026d0f048194634be22307aabf0ad58
#: ../../install_first_access.rst:18
msgid "*Figure 1: hosts file*"
msgstr "*Figure 1: hosts file*"

# a45214406e6a4caf80d93e269ec911e4
#: ../../install_first_access.rst:20
msgid ""
"Al encontrarnos con la pantalla de login, simplemente se debe ingresar el"
" usuario admin y la clave generada durante la instalación, como se expone"
" en la figura 2."
msgstr ""
"When finding the login screen, we just enter the admin user and the "
"password generated during the installation (my_inventory), as shown on "
"figure 2."

# 267a4f4fcaf04ccabbf288dad1713909
#: ../../install_first_access.rst:24
msgid "*Figure 2: First login*"
msgstr "*Figure 2: First login*"
